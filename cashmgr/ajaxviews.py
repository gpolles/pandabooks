from .ajax import *
from .parsing import update_assignment, parse_ofx
from .utils import basic_serialize
from io import TextIOWrapper
from .models import *
from .forms import *
from django.apps import apps
from ofxparse import OfxParser

App = apps.get_app_config('cashmgr')

# ADDING RECORDS

@auth_ajax_view
def process_ofx_file(request):
    form = UploadOfxForm(request.POST, request.FILES)
    if form.is_valid():
        f = TextIOWrapper(request.FILES['file'].file, encoding='ascii', 
                          errors='replace')
        try:
            ofx = OfxParser.parse(f)
            account = Account.objects.filter(user=request.user).get(ofxid=ofx.account.account_id)
            n_tot, n_add = parse_ofx(ofx, account)

        except Account.DoesNotExist:
            return ajax_error( 'InvalidOfxId', additional_data={'ofxid': ofx.account.account_id} )
        
        return ajax_confirm({'n_add': n_add, 'n_tot': n_tot })
        
    else:
        return ajax_error('FormNotValid')


@auth_ajax_view
def add_payee_execute(request):

    form = PayeeForm(request.POST)
    
    if form.is_valid():
    
        transaction_id = -1
        categories = Category.objects.filter(user=request.user)
        transactions = Transaction.objects.filter(user=request.user)

        if Payee.objects.filter( user=request.user, name=request.POST.get('name') ).first() is not None:
            return ajax_error('A payee with this name already exists')
    
        default_category_str = request.POST.get('default_category')
        dc = categories.filter(name=default_category_str).first()
    
        if dc is None and default_category_str not in [None, '', 'None']:
            return ajax_error('CategoryDoesNotExist')

        payee = form.save(commit=False)
        payee.user = request.user
        payee.default_category = dc
        payee.save()

        try:
            transaction_id = int(request.POST.get("transaction"))
            transaction = transactions.get(pk=transaction_id)
            transaction.payee = payee
            if transaction.category is None:
                transaction.category = payee.default_category
            transaction.save()
        except:
            transaction_id = -1

        assigned = update_assignment(transactions, payee)

        if transaction_id != -1:
            assigned += [transaction]

        # create updates to send back 
        targets = []
        targets.append( basic_serialize(payee) )
        for t in assigned:
            targets.append( basic_serialize(t) )
            
        return ajax_confirm( { 'request': 'update', 'targets': targets } )

    return ajax_error('InvalidForm', form.errors)


@auth_ajax_view
def add_category_execute(request):

    categories = Category.objects.filter(user=request.user)
    transactions = Transaction.objects.filter(user=request.user)
    
    form = CategoryForm(request.POST)

    if form.is_valid():

        category = form.save(commit=False)            
        category.user = request.user
        vals = category.name.split(':')
        
        # Create category tree and update child/parent relationships
        import sys
        c = None
        for i in range(len(vals)-1):
            cname = ':'.join(vals[:i+1])
            uname = ':'.join(vals[:i])
            print ('cname', cname, 'uname', uname, 'cnt', categories.filter(name=cname).count(), file=sys.stderr)
            if categories.filter(name=cname).count() == 0:
                if uname == '':
                    sup = None
                else:
                    sup = categories.get(name=uname)
                c = Category(name=cname, parent_category=sup, user=request.user)
                c.save()
                categories = Category.objects.filter(user=request.user)
            else:
                c = categories.get(name=cname)
        
        category.parent_category = c
        category.save() 

        try:
            transaction_id = int(request.POST.get("transaction"))
            transaction = transactions.get(pk=transaction_id)
            transaction.category = category
        except:
            transaction_id = -1

        targets = []
        targets.append( basic_serialize(category) )
        
        if transaction_id != -1:
            targets.append( basic_serialize(transaction) )
        
        return ajax_confirm({ 'request': 'update', 'targets': targets })

    return ajax_error('InvalidForm', form.errors)


# MODIFY RECORDS

@auth_ajax_view
def modify_payee_execute(request, uid):

    categories = Category.objects.filter(user=request.user)
    transactions = Transaction.objects.filter(user=request.user)
    payees = Payee.objects.filter(user=request.user)

    payee = payees.get(pk=uid)
    old_category = payee.default_category
    old_lookup = payee.lookup_strings

    form = PayeeForm(request.POST, instance=payee)
    update_transactions = request.POST.get('update_transactions', False)

    if form.is_valid():
        
        default_category_str = request.POST.get('default_category')
        dc = categories.filter(name=default_category_str).first()

        # returns error if the category does not exists.
        if dc is None and default_category_str not in [None, '', 'None']:
            return ajax_error('InvalidCategory', 'no Category: %s' % default_category_str)

        payee = form.save(commit=False)
        payee.default_category = dc
        payee.user = request.user
        payee.save()

        targets = []  # update targets for javascript
        assigned = []
        if old_lookup != payee.lookup_strings:
            assigned = update_assignment(transactions, payee)
            targets.append( basic_serialize(payee) )

        if old_category != payee.default_category:
            if update_transactions:
                for t in payee.transaction_set.exclude(category=payee.default_category):
                    t.category = payee.default_category
                    t.incomplete = t.is_incomplete()
                    t.save()
                    assigned.append(t)
            
        # create updates to send back 
        for t in assigned:
            targets.append( basic_serialize(t) )

        return ajax_confirm({ 'request': 'update', 'targets': targets})

    return ajax_error('InvalidForm', form.errors)


@auth_ajax_view
def modify_transaction_inline(request, transaction_id):

    transaction = Transaction.objects.filter( user=request.user, pk=transaction_id ).first()
    if transaction is None:
        return ajax_error('TransactionDoesNotExist')


    name = request.GET.get('payee')

    if name not in [None, '', 'None']:
        payee = Payee.objects.filter( user=request.user, name=name ).first()
        if payee is None:
            return ajax_error('PayeeDoesNotExist')
        transaction.payee = payee
        if payee.default_category is not None:
            transaction.category = payee.default_category
        transaction.save()
        return ajax_confirm({ 
            'request': 'update', 
            'targets': [ basic_serialize(transaction) ]
        })

    name = request.GET.get('category')

    if name not in [None, '', 'None']:
        category = Category.objects.filter( user=request.user, name=name ).first()
        if category is None:
            return ajax_error('CategoryDoesNotExist')
        transaction.category = category
        transaction.save()
        return ajax_confirm({ 
            'request': 'update', 
            'targets': [ basic_serialize(transaction) ]
        })

    return ajax_error('EmptyString')


def payee_change(request, transaction_id):

    payees = Payee.objects.filter(user=request.user)
    payee_name = request.GET.get('payee', 'None')

    try:    
        transaction = Transaction.objects.get(pk=transaction_id)
    except Transaction.DoesNotExist:
        return ajax_error('TransactionDoesNotExist')
        response = json.dumps({
            'status': 'fail',
            'msg': {
                'reason': 'TransactionDoesNotExist',
                'rollback': [
                    {
                        'etype': 'transaction',
                        'uid': transaction_id,
                        'field': 'payee',
                        'value': None
                    },
                ],
            }
        })
        return HttpResponse(response)

    if len(payee_name) == 0  or payee_name=='None':
        response = json.dumps({
            'status': 'fail',
            'msg': {
                'reason': 'EmptyString',
                'rollback': [
                    {
                        'etype': 'transaction',
                        'uid': transaction_id,
                        'field': 'payee',
                        'value': transaction.payee.name
                    },
                ],
            }
        })
        return HttpResponse(json.dumps(response))

    try:
        payee = payees.get(name=payee_name)
        transaction.payee = payee
        if payee.default_category is not None:
            transaction.category = payee.default_category
        if transaction.is_complete():
            transaction.incomplete = False
        transaction.save()


        response = json.dumps({
            'status': 'ok',
            'msg': {
                'request': 'update',
                'targets': [
                    {
                        'etype': 'transaction',
                        'uid': transaction_id,
                        'field': 'payee',
                        'value': payee.name
                    },
                    {
                        'etype': 'transaction',
                        'uid': transaction_id,
                        'field': 'category',
                        'value': transaction.category.name
                    },
                    {
                        'etype': 'transaction',
                        'uid': transaction_id,
                        'field': 'incomplete',
                        'value': transaction.incomplete
                    },
                ],
            }, 
        })
        return HttpResponse(response)

    except Payee.DoesNotExist:
        if transaction.payee is None:
            payee_name = 'None'
        else:
            payee_name = transaction.payee.name
        response = json.dumps({
            'status': 'fail',
            'msg': {
                'reason': 'PayeeDoesNotExist',
                'rollback': [
                    {
                        'etype': 'transaction',
                        'uid': transaction_id,
                        'field': 'payee',
                        'value': payee_name
                    },
                ],
            }
        })
        return HttpResponse(response)


@auth_ajax_view
def category_change(request, transaction_id):
    name = request.GET.get('category', '')

    try:
        transaction = Transaction.objects.get(pk=transaction_id)

    
    except Transaction.DoesNotExist:
        response = json.dumps({
            'status': 'fail',
            'msg': {
                'reason': 'TransactionDoesNotExist',
                'rollback': [
                    {
                        'etype': 'transaction',
                        'uid': transaction_id,
                        'field': 'category',
                        'value': None
                    },
                ],
            }
        })
        return HttpResponse(response)

    if len(name) == 0 or name=='None':
        response = json.dumps({
            'status': 'fail',
            'msg': {
                'reason': 'EmptyString',
                'rollback': [
                    {
                        'etype': 'transaction',
                        'uid': transaction_id,
                        'field': 'category',
                        'value': transaction.category.name
                    },
                ],
            }
        })
        return HttpResponse(response)

    try:
        categories = Category.objects.filter(user=request.user)
        category = categories.get(name=name)
        transaction.category = category
        if transaction.is_complete():
            transaction.incomplete = False
        transaction.save()
        response = json.dumps({
            'status': 'ok',
            'msg': {
                'request': 'update',
                'targets': [
                    {
                        'etype': 'transaction',
                        'uid': transaction_id,
                        'field': 'category',
                        'value': transaction.category.name
                    },
                    {
                        'etype': 'transaction',
                        'uid': transaction_id,
                        'field': 'incomplete',
                        'value': transaction.incomplete
                    },
                ],
            }, 
        })
        return HttpResponse(response)

    except Category.DoesNotExist:
        response = json.dumps({
            'status': 'fail',
            'msg': {
                'reason': 'CategoryDoesNotExist',
                'rollback': [
                    {
                        'etype': 'transaction',
                        'uid': transaction_id,
                        'field': 'category',
                        'value': transaction.category.name
                    },
                ],
            }
        })
        return HttpResponse(response)


# EXISTANCE CHECKS

@auth_ajax_view
def payee_exists(request):
    payees = Payee.objects.filter(user=request.user)
    try:
        name = request.GET.get('name') 
        c = payees.filter( name=name ).first()
        response = ajax_confirm( basic_serialize(c) )
    except Payee.DoesNotExist:
        response = ajax_error('PayeeDoesNotExist')
    return HttpResponse(response)


@auth_ajax_view
def category_exists(request):
    categories = Category.objects.filter(user=request.user)
    try: 
        name = request.GET.get('name')
        c = categories.filter( name=name ).first()
        response = ajax_confirm( basic_serialize(c) )
    except Category.DoesNotExist:
        response = ajax_error('CategoryDoesNotExist')
    return HttpResponse(response)

@auth_ajax_view
def query_record(request):
    origin = request.POST if request.method == 'POST' else request.GET
    model = origin.get('model')
    field = origin.get('field')
    value = origin.get('value')
    if model in ['Payee', 'Category', 'Invoice']:
        record = App.get_model( model ).objects.filter(user=request.user, **{field: value}).first()
        if record is not None:
            return ajax_confirm( basic_serialize(record) )
        return ajax_error('RecordNotFound')
    return ajax_error('InvalidArguments')
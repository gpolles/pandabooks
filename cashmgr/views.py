import json
import pytz
import datetime
from dateutil.relativedelta import relativedelta


from django.shortcuts import render, get_object_or_404
from django.db.models import Min
from django.http import HttpResponse
from django.utils.safestring import mark_safe
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from .forms import AccountForm, PayeeForm, CategoryForm, InvoiceForm, UploadOfxForm
from .parsing import update_assignment
from .models import Account, Payee, Category, Transaction, Invoice
from .utils import *
from .pdfs_generation import generate_invoice
from .ajaxviews import *


from django.contrib.auth.decorators import login_required

TRANSACTION_TYPES = ['check', 'other', 'transfer', 'debit', 'credit']
PAGINATOR_SHOW_ITEMS_MAX = 11


def json_data(request = None):
    data = {
        'payee_names': list(Payee.objects.filter(user=request.user).values_list('name', flat=True).order_by('name')),
        'category_names': list(Category.objects.filter(user=request.user).values_list('name', flat=True).order_by('name')),
    }
    return json.dumps(data)


@login_required(login_url='/cashmgr/accounts/login/')
def index(request):
    accounts = Account.objects.filter(user=request.user)

    transactions = Transaction.objects.filter(user=request.user)
    total_income = totsum(transactions.filter(amount__gt=0))
    total_expenses = -totsum(transactions.filter(amount__lt=0))

    context = {'accounts': accounts,
               'total_balance': total_income - total_expenses,
               'total_income': total_income,
               'total_expenses': total_expenses,
               'total_transactions': transactions.filter(user=request.user).count()}
    return render(request, 'cashmgr/index.html', context)


@login_required(login_url='/cashmgr/accounts/login/')
def add_account_dialog(request):
    if request.method == 'GET':
        ofxid = request.GET.get('ofxid')
        context = {
            'form': AccountForm(),
            'ofxid': ofxid
        }
        return render(request, 'cashmgr/forms/add_account.html', context)


@login_required(login_url='/cashmgr/accounts/login/')
def add_account(request):
    if request.method == 'POST':
        form = AccountForm(request.POST)
        if form.is_valid():
            form = form.save(commit=False)
            form.user = request.user
            form.save()
            return HttpResponse('<script type="text/javascript">'
                                'window.opener.$("form#ofxform").trigger( "submit" );'
                                'window.close(); '
                                '</script>')


@login_required(login_url='/cashmgr/accounts/login/')
def add_payee(request):

    if request.method == 'POST':
        return add_payee_execute(request)

    else:

        rname = request.GET.get('name')
        transaction_id = request.GET.get('transaction')
        
        try:

            transaction = Transaction.objects.get(user=request.user, pk=transaction_id)
            description = transaction.description

        except:
            
            description = ''

        form = PayeeForm(initial={'name': rname,
                                  'lookup_strings': description})

    return render(request,
                  'cashmgr/forms/add_payee.html',
                  {
                    'form': form,
                    'categories': Category.objects.filter(user=request.user),
                    'payees': Payee.objects.filter(user=request.user),
                    'transaction': transaction_id,
                    'description': description
                  })


@login_required(login_url='/cashmgr/accounts/login/')
def add_category(request):

    if request.method == 'POST':
        
        return add_category_execute(request)

    else:

        rname = request.GET.get('name')
        
        if rname is not None:

            form = CategoryForm(initial={'name': rname})
        
        else: 
        
            form = CategoryForm()
        
        transaction_id = request.GET.get('transaction')
    
    context = {
        'form': form,
        'categories': Category.objects.filter(user=request.user),
        'transaction': transaction_id
    }
    return render(request, 'cashmgr/forms/add_category.html', context) 


@login_required(login_url='/cashmgr/accounts/login/')
def modify_payee(request, uid):
    
    if request.method == 'POST':
        
        return modify_payee_execute(request, uid)

    else:
        
        payee = get_object_or_404(Payee.objects.filter(user=request.user), pk=uid)
        form = PayeeForm(instance=payee)
        return render(request,
                      'cashmgr/forms/modify_payee.html',
                      {'form': form,
                       'jsdata': mark_safe(json_data(request)),
                       'payee': payee,
                       'default_category': payee.default_category})


@login_required(login_url='/cashmgr/accounts/login/')
def upload_file(request):

    if request.method == 'POST':
        
        return process_ofx_file(request)

    else:
        form = UploadOfxForm()
        return render(request, 'cashmgr/forms/upload.html', {'form': form})


@login_required(login_url='/cashmgr/accounts/login/')
def account_view(request, account_id):
    account = Account.objects.get(pk=account_id)

    ob = request.GET.get('ob')
    obd = request.GET.get('obd')
    if ob is None:
        ob = 'date'
        obd = '-' 
    if obd is None:
        obd = ''

    if obd == '':
        iobd = '-'
    else:
        iobd = ''

    transaction_list = account.transaction_set.order_by(obd + ob)
    paginator = Paginator(transaction_list, 50) 

    page = request.GET.get('page')
    try:
        transactions = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        transactions = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        transactions = paginator.page(paginator.num_pages)

    return render(request, 'cashmgr/account_view.html',
                  {'transactions': transactions,
                   'account': account,
                   'obd': obd, 'ob': ob, 'iobd': iobd,
                   'jsdata': mark_safe(json_data(request))})



def quick_add(request, transaction_id):
    try:
        t = Transaction.objects.get(pk=transaction_id)
    except:
        return HttpResponse(json.dumps('{status: "fail", msg: "no id"}'))

    if t.payee is not None:
        return HttpResponse(json.dumps('{status: fail, msg: "has payee"}'))

    if t.category is None:
        try:
            uph = UserParamHolder.objects.filter(user=request.user)[0]
            qa_category = uph.quick_add_category
            if qa_category is None:
                raise ValueError()
        except:
            return HttpResponse(json.dumps('{status: fail, msg: "category lookup"}'))
    else:
        qa_category = t.category

    p = Payee(name=t.suggested_payee(),
              lookup_strings=t.suggested_lookup(),
              default_category=qa_category)
    
    p.save()
    t.payee = p
    t.category = p.default_category
    t.save()
    
    update_assignment(p)

    for t in p.transaction_set.filter(user=request.user):
        if t.is_complete():
            t.incomplete = False
            t.save()
    return HttpResponse(json.dumps('{status: ok }'))

  
def incomplete_transactions(request):
    ob = request.GET.get('ob')
    obd = request.GET.get('obd')
    if ob is None:
        ob = 'date'
        obd = '-' 
    if obd is None:
        obd = ''

    if obd == '':
        iobd = '-'
    else:
        iobd = ''

    transaction_list = Transaction.objects.filter(incomplete=True).order_by(obd + ob)
    paginator = Paginator(transaction_list, 50) 

    page = request.GET.get('page')
    try:
        transactions = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        transactions = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        transactions = paginator.page(paginator.num_pages)

    quick_add_category = UserParamHolder.objects.get(pk=1).quick_add_category
    
    try:
        quick_add_category = UserParamHolder.objects.get(pk=1).quick_add_category
    except:
        return HttpResponse('Error with quick add category')

    return render(request, 'cashmgr/incomplete_transactions.html',
                  {'transactions': transactions,
                   'quick_add_category': quick_add_category,
                   'obd': obd, 'ob': ob, 'iobd': iobd,
                   'jsdata': mark_safe(json_data(request))})


@login_required(login_url='/cashmgr/accounts/login/')
def list_transactions(request):
    query = Transaction.objects.filter(user=request.user)
    query, orderinfo = apply_ordering(query, request.GET, 
                                      default='date',
                                      defaultdir='-')

    query, filterinfo = apply_filters(query, request.GET)
    filter_get_str = '&'.join(['%s=%s' % (k, v) for k, v in filterinfo.items()])
    filter_pretty = ' AND '.join(['%s=%s' % (k.replace('filter__', ''), v) for k, v in filterinfo.items()])
    if filter_pretty == '':
        filter_pretty = 'None'

    paginator = Paginator(query, 50) 
    
    page = request.GET.get('page')
    try:
        transactions = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        transactions = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        transactions = paginator.page(paginator.num_pages)

    context = {
        'transactions': transactions,
        'filter_get_str': mark_safe(filter_get_str),
        'filters': filter_pretty,
        'accounts' : Account.objects.filter(user=request.user).order_by('name'),
        'ttypes': TRANSACTION_TYPES,
        'categories': Category.objects.filter(user=request.user).order_by('name'),
        'payees': Payee.objects.filter(user=request.user).order_by('name'),
        'page_list': get_neigh_pages(transactions.number, 
                                     PAGINATOR_SHOW_ITEMS_MAX,
                                     paginator.num_pages),
    }
    context.update(orderinfo)
    context.update(filterinfo)

    return render(request, 'cashmgr/transaction_list.html', context)



@login_required(login_url='/cashmgr/accounts/login/')
def create_report(request):
    import sys
    if request.method == 'POST':
        # parse the filters
        query = Transaction.objects.filter(user=request.user)

        # filter accounts

        filter_account = request.POST.getlist('filter-account[]')
        account_list = Account.objects.filter(id__in=filter_account)
        query = query.filter(account__in=account_list)

        # filter payees
        # id_list = request.POST.getlist('filter-payee')
        # payee_list = Payee.objects.filter(id__in=id_list)
        # if len(payee_list) != 0:
        #     query = query.filter(payee__in=payee_list)

        # filter categorys
        
        filter_category = request.POST.getlist('filter-category')
        # we receive only leaf categories, we have to figure out if we
        # have to include also parents. We include parents if all the children have been 
        # selected. 

        unvisited_children = {}
        cpool = Category.objects.filter(user=request.user)
        selected_categories = []

        # see if all children have been selected
        for f in filter_category:
            cc = cpool.get(pk=f)
            selected_categories.append(cc)
            parent_id = cc.parent_category_id
            # traverse tree upwards and keep track of unvisited children
            # for each ancestor node
            while parent_id is not None:
                pc = cpool.get(pk=parent_id)
                if parent_id not in unvisited_children:
                    unvisited_children[parent_id] = set([x.id for x in pc.subcategories.all()])
                if cc.id in unvisited_children[parent_id]:
                    unvisited_children[parent_id].remove(cc.id)
                cc = pc
                parent_id = cc.parent_category_id
                
        for cid, unvisited in unvisited_children.items():
            if len(unvisited) == 0:
                selected_categories.append(cpool.get(pk=cid))

        
        #category_list.get(pk=101)
        query = query.filter(category__in=selected_categories)
        try:
            print('cat:', file=sys.stderr)
            query.get(pk=6096)
            print('ok', file=sys.stderr)
        except:
            print('not here!!!', file=sys.stderr)
        
        # filter dates
        try: 
            filter_date_from = parse_aware(request.POST.get('filter-date-from'))
            query = query.filter(date__gte=filter_date_from)
        except: 
            filter_date_from = Transaction.objects.filter(user=request.user).aggregate(Min('date'))['date__min']
        try: 
            filter_date_to = parse_aware(request.POST.get('filter-date-to')) 
            query = query.filter(date__lt=filter_date_to + datetime.timedelta(days=1))
        except: 
            filter_date_to = datetime.datetime.now(pytz.timezone('US/Pacific'))

        # filter types
        try:
            filter_ttype = request.POST.getlist('filter-ttype[]')
            ttype_list = [tt for tt in filter_ttype if tt in TRANSACTION_TYPES]
            query = query.filter(ttype__in=ttype_list)
        except:
            pass

        # filter timeview
        filter_timeview = request.POST.get('filter-timeview', None)

        # Prepare basic queries
        hits = query.count()
        if hits == 0:
            return render(request, 'cashmgr/report.html', {'hits': -1})
        expenses = query.filter(amount__lt=0)
        income = query.filter(amount__gt=0)

        total_expenses = -totsum(expenses)
        total_income = totsum(income)
        if abs(total_expenses) < 0.01:
            total_expenses = 0
        if abs(total_income) < 0.01:
            total_income = 0

        # create time series
        # check how large is the query
        if filter_timeview is None:  # auto decide the type of the view
            date_span = ((filter_date_to - filter_date_from).total_seconds() 
                         / 3600 / 24) # number of days
            if date_span < 21:  # below 3 weeks, we organize by day
                filter_timeview = 'byday'
            elif date_span < 90:
                filter_timeview = 'byweek'
            else:
                filter_timeview = 'bymonth'

        def get_time_series(expenses, income, start_date, end_date, 
                            filterfunc, rdkw, strfmt):
            curr_date = start_date
            time_serie_expenses = []
            time_serie_income = []
            while curr_date < end_date + relativedelta(**{rdkw : 1}):
                curr_exp = -totsum(filterfunc(expenses, curr_date))
                curr_inc = totsum(filterfunc(income, curr_date))
                curr_datestr = curr_date.strftime(strfmt)
                time_serie_expenses.append([curr_datestr, curr_exp])
                time_serie_income.append([curr_datestr, curr_inc])
                curr_date = curr_date + relativedelta(**{rdkw : 1})
            return time_serie_expenses, time_serie_income

        ts_parm = {'byday': (same_day, 'days', '%d %b %y'),
                   'byweek': (same_week, 'weeks', '%W %y'),
                   'bymonth': (same_month, 'months', '%b %y')}
        ts_expenses, ts_income = get_time_series(expenses,
                                                 income,
                                                 filter_date_from,
                                                 filter_date_to,
                                                 ts_parm[filter_timeview][0],
                                                 ts_parm[filter_timeview][1],
                                                 ts_parm[filter_timeview][2])


        by_category = {}
        for category in selected_categories:
            vout = -totsum(expenses.filter(category=category))
            vin = totsum(income.filter(category=category))
            if vin == vout == 0.00:
                continue
            by_category[category.name] = {}
            by_category[category.name]['out'] = vout
            by_category[category.name]['in'] = vin
            by_category[category.name]['tot'] = vin - vout

        context = {'accounts': Account.objects.filter(user=request.user),
                   'categorys': Category.objects.filter(user=request.user),
                   'ttypes': TRANSACTION_TYPES,
                   'ttype_list': ttype_list,
                   'category_list': selected_categories,
                   'account_list': account_list,
                   'jsdata': mark_safe(json_data(request)),
                   'min_date': filter_date_from.strftime('%d %B %Y'),
                   'max_date': filter_date_to.strftime('%d %B %Y'),
                   'total_income': total_income,
                   'total_expenses': total_expenses,
                   'grand_total': total_income - total_expenses,
                   'json_by_category': mark_safe(json.dumps(by_category)),
                   'by_category': by_category,
                   'ts_expenses': mark_safe(json.dumps(ts_expenses)),
                   'ts_income': mark_safe(json.dumps(ts_income)),
                   'hits': hits}
        return render(request, 'cashmgr/report.html', context)

    return render(request, 'cashmgr/report.html', {})

@login_required(login_url='/cashmgr/accounts/login/')
def report_center(request):
    context = {'accounts': Account.objects.filter(user=request.user),
               'categorys': Category.objects.filter(user=request.user).order_by('name'),
               'ttypes' : TRANSACTION_TYPES,
               'filter_ttype': TRANSACTION_TYPES,
               'filter_category': [c.id for c in Category.objects.filter(user=request.user)],
               'filter_account': [a.id for a in Account.objects.filter(user=request.user)],
               'jsdata': mark_safe(json_data(request))}
    return render(request, 'cashmgr/report_center.html', context)

@login_required(login_url='/cashmgr/accounts/login/')
def payee_center(request):

    orderby = request.GET.get('orderby', 'name')
    orderdir = request.GET.get('orderdir', 'des')
    dirc = '' if orderdir == 'asc' else '-' 

    if callable( getattr(Payee, orderby, None) ):
        payees = sorted( 
            Payee.objects.filter(user=request.user), 
            key=lambda x: getattr(x, orderby)(),
            reverse=(orderdir == 'des')
        )
    else:        
        payees = Payee.objects.filter(user=request.user).order_by(dirc + orderby)
    
    paginator = Paginator(payees, 50) 
    
    page = request.GET.get('page')
    try:
        page_payees = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        page_payees = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        page_payees = paginator.page(paginator.num_pages)
    
    context = {'page_payees': page_payees,
               'payees': payees,
               'page_list': get_neigh_pages(page_payees.number, 
                                            PAGINATOR_SHOW_ITEMS_MAX,
                                            paginator.num_pages),
               }
    return render(request, 'cashmgr/payee_list.html', context)


@login_required(login_url='/cashmgr/accounts/login/')
def add_invoice(request, pk=-1):
    if request.method == 'POST':
        try:
            payee = Payee.objects.get(name=request.POST.get('payee'))
            items = json.loads(request.POST.get('items'), 'None')
            target_id = int(request.POST.get('target'))
            if target_id == -1:
                form = InvoiceForm(request.POST)
                updreq = 'add'
            else:
                target = Invoice.objects.get(pk=target_id)
                form = InvoiceForm(request.POST, instance=target)
                updreq = 'update'
            if form.is_valid():
                invoice = form.save(commit=False)
                invoice.payee = payee
                invoice.details = json.dumps(items)
                invoice.amount = sum([i['unit_price']*i['quantity'] for i in items])
                invoice.open_balance = invoice.amount
                invoice.save()

                response = json.dumps({
                    'status': 'ok',
                    'msg': {
                        'request': updreq,
                        'targets': [
                            {
                                'etype': 'invoice',
                                'uid': invoice.id,
                                'field': 'all',
                                'value': None,
                            }
                        ],
                    }

                })
                return HttpResponse(response)
            else:
                raise ValueError()
        except:
            response = json.dumps({
                'status': 'fail',
                'msg': {
                    'reason': 'YouFuckedUp',
                }
            })
            raise # for now just raise

    try:
        invoice = Invoice.objects.get(pk=pk)
        model_invoice = invoice
    except Invoice.DoesNotExist:
        invoice = None
        try:
            model_invoice = Invoice.objects.get(pk=request.GET.get('duplicate'))
        except Invoice.DoesNotExist:
            model_invoice = None

    form = InvoiceForm(instance=model_invoice)

    context = {
        'target_invoice': invoice,
        'form': form,
        'jsdata': mark_safe(json_data(request)),
        'model_invoice': model_invoice
    }

    return render(request, 'cashmgr/forms/invoice_details.html', context)


@login_required(login_url='/cashmgr/accounts/login/')
def show_payee(request, uid):
    payee = get_object_or_404(Payee, pk=uid) 
    open_balance = sum([i.open_balance for i in payee.invoice_set.filter(user=request.user)])
    ts = payee.transaction_set.filter(user=request.user).order_by('-date')
    invs = payee.invoice_set.filter(user=request.user).order_by('-date_created')
    context = {
        'transactions': ts,
        'n_transactions': ts.count(),
        'invoices': invs,
        'n_invoices': invs.count(),
        'open_balance': open_balance,
        'payee': payee,
        'jsdata': mark_safe(json_data(request)),
    }
    return render(request, 'cashmgr/payee_view.html', context)


@login_required(login_url='/cashmgr/accounts/login/')
def invoice_list(request):
    query = Invoice.objects.filter(user=request.user)
    query, orderinfo = apply_ordering(query, request.GET, 
                                      default='date_created',
                                      defaultdir='-')

    query, filterinfo = apply_filters(query, request.GET)
    filter_get_str = '&'.join(['%s=%s' % (k, v) for k, v in filterinfo.items()])

    paginator = Paginator(query, 50) 
    
    page = request.GET.get('page')
    try:
        invoices = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        invoices = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        invoices = paginator.page(paginator.num_pages)

    context = {'invoices': invoices,
               'filter_get_str': mark_safe(filter_get_str),
               'payees': Payee.objects.filter(user=request.user).order_by('name'),
               'page_list': get_neigh_pages(invoices.number, 
                                            PAGINATOR_SHOW_ITEMS_MAX,
                                            paginator.num_pages),
               'jsdata': mark_safe(json_data(request))}
    context.update(orderinfo)
    context.update(filterinfo)

    return render(request, 'cashmgr/invoice_list.html', context)


@login_required(login_url='/cashmgr/accounts/login/')
def change_field(request, model, pk, ftype, field):
    response = ''
    if request.method == 'POST':
        try:
            value = request.POST['value']
            O = apps.get_model(app_label='cashmgr', model_name=model)
            obj = get_record_by_pk_or_name(model, pk);
            oldval = getattr(obj, field)
            if ftype == 'text':
                setattr(obj, field, value)
            elif ftype == 'date':
                setattr(obj, field, parse_aware(value))
            elif ftype == 'model':
                omn, uid = value.split(':')
                om = apps.get_model(app_label='cashmgr', 
                                    model_name=omn).objects.get(pk=uid)
                setattr(obj, field, om)
            else:
                raise ValueError()
            obj.save()
            response = json.dumps({
                'status': 'ok',
            })
        except:
            response = json.dumps({
                'status': 'fail',
                'msg': {}
            })
    return HttpResponse(response)


def preview_invoice(request):
    try:
        payee = Payee.objects.get(name=request.GET.get('payee'))
        items = json.loads(request.GET.get('items'), 'None')
        form = InvoiceForm(request.GET)
        if form.is_valid():
            invoice = form.save(commit=False)
            invoice.payee = payee
            invoice.details = json.dumps(items)
            invoice.amount = sum([i['unit_price']*i['quantity'] for i in items])
            invoice.open_balance = invoice.amount
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'filename="invoice%s.pdf"' % invoice.id
        response.write(generate_invoice(invoice))
    except:
        response = HttpResponse('We got an error.')
    return response
    

@login_required(login_url='/cashmgr/accounts/login/')
def delete_transaction(request, transaction_id):
    try:
        t = Transaction.objects.get(pk=transaction_id)
        t.delete()
        response = json.dumps({
            'status': 'ok',
            'msg': ''
        })
    except:
        response = json.dumps({
            'status': 'fail',
            'msg': {}
        })
    return HttpResponse(response)

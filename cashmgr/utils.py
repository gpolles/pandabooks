from django.db.models import Sum
from django.forms.models import model_to_dict
from django.apps import apps
from django.utils.dateparse import parse_date
import pytz
from datetime import datetime, timedelta
import json


def n2z(x):
    if x is None:
        return 0
    return x


def totsum(query):
    return n2z(query.aggregate(Sum('amount'))['amount__sum'])


def same_day(query, cdate):
    return (query.filter(date__day=cdate.day)
                 .filter(date__month=cdate.month)
                 .filter(date__year=cdate.year))


def same_month(query, cdate):
    return (query.filter(date__year=cdate.year)
                 .filter(date__month=cdate.month))


def same_week(query, cdate):
    start_week = cdate - timedelta(cdate.weekday())
    end_week = start_week + timedelta(7)
    return (query.filter(date__range=[start_week, end_week]))


def same_year(query, cdate):
    return query.filter(date__year=cdate.year)


def parse_aware(dstring, mytz='US/Pacific'):
    naive = parse_date(dstring)
    naive = datetime.combine(naive, datetime.min.time())

    return pytz.timezone(mytz).localize(naive, is_dst=None)


def get_neigh_pages(current, n_pages, n_total):
    if n_pages > n_total:
        return [i for i in range(1, n_total + 1)]
    while current - n_pages // 2 < 1:
        current += 1
    while current + n_pages // 2 > n_total:
        current -= 1
    return [i for i in range(current - n_pages // 2,
                             current + n_pages // 2 + 1)]


def filter_field(query, field, operator, value, field_type='str'):
    '''a None value means don't filter'''
    if field_type == 'date':
        value = parse_aware(value)

    if operator in ['eq', '=', '==']:
        query = query.filter(**{field: value})
    elif operator in ['gt', '>', 'from']:
        query = query.filter(**{field + '__gt': value})
    elif operator in ['lt', '<', 'to']:
        query = query.filter(**{field + '__lt': value})
    elif operator in ['id']:
        query = query.filter(**{field: int(value)})

    return query


def apply_filters(query, vdict):
    applied_filters = {}
    for key, val in vdict.items():
        if val is None or val == '' or val == 'None':
            continue

        if 'filter__' != key[:8]:
            continue

        cvals = key[8:].split('__')

        field = None
        field_type = 'str'
        operator = 'eq'

        field = cvals[0]
        if 'date' in field:
            field_type = 'date'
        
        try: 
            operator = cvals[1]
        except:
            pass 

        query = filter_field(query, field, operator, val, field_type)
        applied_filters[key] = val
    return query, applied_filters


def apply_ordering(query, vdict, default=None, defaultdir=''):
    '''
    defaultdir is either '-' or ''
    '''
    def invert(od):
        if od == '-':
            iod = ''
        else:
            iod = '-'
        return iod 

    field = vdict.get('orderby', None)
    if field is None:
        if default is None:
            return {'orderby': None, 
                    'orderdir': defaultdir, 
                    'iorderdir': invert(defaultdir)}
        field = default

    odir = vdict.get('orderdir')
    if odir is None:
        odir = defaultdir

    
    return (query.order_by(odir + field),
            {'orderby': field, 'orderdir': odir, 'iorderdir': invert(odir)})
    

class JSONResponse(object):
    """docstring for Response"""
    def __init__(self, status, rtype='SomethingIsWrong', errdata=''):
        super(JSONResponse, self).__init__()
        self.status = status
        if status == 'fail':
            self.struct = { 
                'status': 'fail',
                'msg': {
                    'reason': rtype,
                    'data': errdata,
                    'rollback': []
                }
            }
        else:
            self.struct = { 
                'status': status,
                'msg': {
                    'request': rtype,
                    'targets': []
                }
            }

    def add_target(self, subclass='targets', **kwargs):
        if subclass not in self.struct['msg'].keys():
            self.struct['msg'][subclass] = []
        self.struct['msg']['targets'].append(kwargs)

    def update_transactions(self, transactions, fields, subclass='target'):
        for t in transactions:
            for f in fields:
                target = {
                    'etype': 'transaction', 
                    'uid': t.id,
                    'field': f,
                    'value': str(t.getattr(self, f))
                }
                self.add_target(subclass=subclass, **target)

    def rollback_transactions(self, transactions, fields):
        self.update_transactions(transactions, fields, 'rollback')

    def __str__(self):
        return json.dumps(self.struct)

    def serialize(self):
        return json.dumps(self.struct)


def get_record_by_pk_or_name(model, pk):
    try:    
        O = apps.get_model(app_label='cashmgr', model_name=model)
        try:
            ipk=int(pk)
        except:
            ipk = None
        if ipk is not None:
            obj = O.objects.get(pk=ipk)
        else:  # maybe a name identifier was sent?
            obj = O.objects.get(name=pk)
    except:
        obj = None
    return obj

def get_first(group, default=None, **kwargs):
    el = group.filter(**kwargs).first()
    if el is None:
        return default
    return el

def basic_serialize(obj):
    md = model_to_dict(obj)
    for key, val in md.items():
        if isinstance(val, datetime):
            md[key] = str(val)
    md['model'] = obj.__class__.__name__
    return md

def json_basic_serialize(obj):
    return json.dumps( basic_serialize(obj) )
    
    
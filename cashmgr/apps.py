from django.apps import AppConfig


class CashmgrConfig(AppConfig):
    name = 'cashmgr'

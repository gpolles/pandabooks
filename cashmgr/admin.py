from django.contrib import admin

# Register your models here.
from .models import *

# The django doc has an example that does almost exactly what you want:

# https://docs.djangoproject.com/en/1.11/ref/contrib/admin/#django.contrib.admin.ModelAdmin.get_queryset

# The idea is to override the get_queryset() method in the model admin view:

# # admin.py
# from django.contrib import admin

# class YourModelAdmin(admin.ModelAdmin):
#     def get_queryset(self, request):
#         qs = super(MyModelAdmin, self).get_queryset(request)
#         return qs.filter(author=request.user)

# admin.site.register(YourModel, YourModelAdmin)

class UserLimitedAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        qs = super(UserLimitedAdmin, self).get_queryset(request)
        if request.user.is_superuser:
        	return qs
        else:
        	try:
        		return qs.filter(user=request.user)
        	except:
        		return qs.none()


admin.site.register(Transaction, UserLimitedAdmin)
admin.site.register(Payee, UserLimitedAdmin)
admin.site.register(Account, UserLimitedAdmin)
admin.site.register(Category, UserLimitedAdmin)
admin.site.register(AccountType, UserLimitedAdmin)
admin.site.register(Invoice, UserLimitedAdmin)
admin.site.register(PaymentType, UserLimitedAdmin)
admin.site.register(Payment, UserLimitedAdmin)
admin.site.register(MyCompany, UserLimitedAdmin)


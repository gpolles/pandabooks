from django.db import models
from django.contrib.auth.models import User
from .utils import totsum, json_basic_serialize
import string
import json
from django.core.serializers import serialize
from django.core.serializers.json import DjangoJSONEncoder
import datetime
import pytz

class JSONField(models.TextField):
    """
    JSONField is a generic textfield that neatly serializes/unserializes
    JSON objects seamlessly.
    Django snippet #1478

    example:
        class Page(models.Model):
            data = JSONField(blank=True, null=True)


        page = Page.objects.get(pk=5)
        page.data = {'title': 'test', 'type': 3}
        page.save()
    """

    def to_python(self, value):
        if value == "":
            return None

        try:
            if isinstance(value, str):
                return json.loads(value)
        except ValueError:
            pass
        return value

    def get_db_prep_save(self, value, *args, **kwargs):
        if value == "":
            return None
        if isinstance(value, dict):
            value = json.dumps(value, cls=DjangoJSONEncoder)
        return super(JSONField, self).get_db_prep_save(value, *args, **kwargs)


TRANSACTION_TYPES = [('check', 'Check'),
                     ('deposit', 'Deposit'),
                     ('transfer', 'Transfer'),
                     ('other', 'Other')]


class Transaction(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    ofxid = models.CharField(max_length=200, default='', editable=False)
    amount = models.FloatField('amount')
    notes = models.CharField(max_length=200, null=True, blank=True)
    date = models.DateTimeField('date')
    ttype = models.CharField(max_length=15,
                             choices=TRANSACTION_TYPES,
                             null=False,
                             default='other')
    category = models.ForeignKey('Category', 
                                 null=True, blank=True, on_delete=models.SET_NULL)
    description = models.CharField(max_length=400, default='')
    payee = models.ForeignKey('Payee', null=True, blank=True, on_delete=models.SET_NULL)
    account = models.ForeignKey('Account', null=True, blank=True, on_delete=models.CASCADE)
    check_number = models.IntegerField('Check #', null=True, blank=True)
    incomplete = models.BooleanField('Incomplete', default=True)

    def is_complete(self):
        if self.payee is None or self.category is None:
            return False
        return True

    def is_incomplete(self):
        return not self.is_complete()

    def suggested_payee(self):
        sp = self.description[:25]
        if len(sp.split()) > 3:
            sp = ' '.join(sp.split()[:3])
        return string.capwords(sp)

    def suggested_lookup(self):
        return self.description[:25]

    def basic_json(self):
        return json_basic_serialize(self)

    def save(self, *args, **kwargs):
        self.incomplete = self.is_incomplete()
        super(Transaction, self).save(*args, **kwargs) # Call the "real"

    def __str__(self):
        return '({}) {} [{:.2f}]'.format(self.date.strftime("%Y-%m-%d"), self.description, self.amount) 


class Account(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    name = models.CharField(max_length=200)
    ofxid = models.CharField(max_length=200, default='')    
    atype = models.ForeignKey('AccountType', on_delete=models.SET_NULL, null=True, blank=True)
    notes = models.CharField(max_length=200, null=True, blank=True)
    check_info = JSONField(blank=True, null=True, default=None)

    def __str__(self):
        return self.name

    def balance(self):
        return self.income() - self.expenses()

    def income(self):
        return totsum(self.transaction_set.filter(amount__gt=0))

    def expenses(self):
        return -totsum(self.transaction_set.filter(amount__lt=0))

    def ntransactions(self):
        return self.transaction_set.count()

    def last_transaction_date(self):
        return self.transaction_set.latest('date').date



class AccountType(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name
    # eventually add the tax line or something


class TransactionType(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name


class Category(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    name = models.CharField(max_length=200)
    parent_category = models.ForeignKey('Category', null=True, blank=True,
                                        related_name='subcategories',
                                        default=None, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    def path(self):
        return '/'.join(self.name.split(':')[:-1])

    def basename(self):
        return self.name.split(':')[-1]

    def basic_json(self):
        return json_basic_serialize(self)

class Payee(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    name = models.CharField(max_length=200)
    default_category = models.ForeignKey('Category', null=True, blank=True, 
                                         on_delete=models.SET_NULL)
    # Comma-separated values
    lookup_strings = models.CharField(max_length=400, default='', blank=True)
    invoice_address = models.TextField(null=True, blank=True, default=None)
    invoice_email = models.EmailField(null=True, blank=True, default=None)

    def __str__(self):
        return self.name

    def balance(self):
        return self.income() - self.expenses()

    def income(self):
        return totsum(self.transaction_set.filter(amount__gt=0))

    def expenses(self):
        return -totsum(self.transaction_set.filter(amount__lt=0))

    def ntransactions(self):
        return self.transaction_set.count()

    def last_transaction_date(self):
        try:
            return self.transaction_set.latest('date').date
        except:
            return datetime.datetime(1, 1, 2, 0, 0, tzinfo=pytz.timezone('UTC'))

    def basic_json(self):
        return json_basic_serialize(self)



class MyCheck(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    number = models.IntegerField(null=True, blank=True)
    payee = models.ForeignKey(Payee, on_delete=models.SET_NULL, null=True)
    amount = models.FloatField()
    date_created = models.DateTimeField()
    date_cashed = models.DateTimeField(null=True, blank=True)
    transaction = models.OneToOneField(Transaction, null=True, 
                                       on_delete=models.SET_NULL, blank=True)
    account = models.ForeignKey(Account, null=True, blank=True, on_delete=models.SET_NULL)
    notes = models.CharField(max_length=100, null=True, blank=True)
    to_print = models.BooleanField(default=False)


class Invoice(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    company = models.ForeignKey('MyCompany', on_delete=models.SET_NULL, default=None, null=True)
    payee = models.ForeignKey(Payee, on_delete=models.SET_NULL, null=True)
    recipient_address = models.TextField(null=True, blank=True, default=None)
    recipient_email = models.EmailField(null=True, blank=True, default=None) 
    amount = models.FloatField()
    date_created = models.DateTimeField()
    date_due = models.DateTimeField(null=True, blank=True)
    date_cashed = models.DateTimeField(null=True, blank=True)
    details = models.TextField(default=[])
    open_balance = models.FloatField()
    paid = models.BooleanField(default=False)
    notes = models.TextField(null=True, blank=True)

    def is_paid(self):
        return self.paid

    def items(self):
        items = json.loads(self.details)
        for i in range(len(items)):
            items[i]['amount'] = items[i]['quantity'] * items[i]['unit_price']
        return items



class PaymentType(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name


class Payment(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    invoice = models.ForeignKey(Invoice, null=True, on_delete=models.SET_NULL)
    ttype = models.ForeignKey(PaymentType, null=True, on_delete=models.SET_NULL)
    amount = models.FloatField()
    date_received = models.DateTimeField()
    notes = models.TextField(null=True, blank=True, default=None)
    transaction = models.ForeignKey(Transaction, null=True, on_delete=models.SET_NULL)


class Items(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    name = models.CharField(max_length=400)
    description = models.TextField(blank=True, null=True)
    unit_amount = models.FloatField()


class UserParamHolder(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    quick_add_category = models.ForeignKey(Category, on_delete=models.SET_NULL, 
                                            null=True, blank=True)


class MyCompany(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    name = models.CharField(max_length=400)
    address = models.TextField(null=True, blank=True, default=None)
    email = models.EmailField(null=True, blank=True, default=None) 
    logo = models.ImageField(null=True, blank=True, default=None)
    invoice_message = models.TextField(null=True, blank=True, default=None)
    invoice_subject = models.CharField(max_length=400, null=True, blank=True, default=None)
    smtp_host = models.CharField(max_length=400, null=True, blank=True, default=None)
    smtp_username = models.CharField(max_length=400, null=True, blank=True, default=None)
    smtp_password = models.CharField(max_length=400, null=True, blank=True, default=None)
    
    
#class ModHistory(models.Model):
#    target = models.CharField(max_length=200)
#    time = models.DateTimeField()
    



import pdfkit
import json
from django.template import loader, Context
from datetime import datetime
import smtplib
from smtplib import SMTP_SSL as SMTP
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email.mime.multipart import MIMEMultipart
from email import encoders


def generate_invoice(invoice):
    
    t = loader.get_template('cashmgr/print_invoice.html')
    
    c = Context({
        'invoice': invoice,
        'now': datetime.now()
    })
    rendered = t.render(c)

    return pdfkit.from_string(rendered, False)


def create_email(subject, my_address, recipients, message, pdfdata, alt_fname):
    
    # Create the container (outer) email message.
    outer = MIMEMultipart()
    outer['Subject'] = subject
    
    outer['From'] = my_address
    outer['To'] = ', '.join(recipients)
    outer.preamble = subject

    msg = MIMEBase('application', 'octet-stream')
    msg.set_payload(pdfdata)
    # Encode the payload using Base64
    encoders.encode_base64(msg)
    # Set the filename parameter
    msg.add_header('Content-Disposition', 'pdfdata', filename=alt_fname)
    outer.attach(msg)

    msg = MIMEText(message, 'plain')
    outer.attach(msg)
    return outer.as_string()


def send_invoice(subject, message, invoice):
    sender = invoice.company
    sender_addr = sender.email
    username = sender.smtp_options.username
    pwd = sender.smtp_options.password
    host = sender.smtp_options.host
    destinations = invoice.recipient_email.split(', ')
    pdfdata = generate_invoice(invoice)
    mail = create_email(subject, 
                        sender_addr,
                        invoice.recipient_email,
                        message,
                        pdfdata,
                        'invoice%s.pdf' % invoice.id)
    try:         
        conn = SMTP(host)
        conn.set_debuglevel(False)
        conn.login(username, pwd)
        try:
            conn.sendmail(sender_addr, destinations, mail)
        finally:
            conn.quit()

    except Exception as exc:
        return str(exc) # give a error message
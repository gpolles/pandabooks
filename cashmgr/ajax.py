import json
from django.http import HttpResponse
import traceback

def ajax_error( etype='DefaultError', description='Internal server error', 
                additional_data={} ):
    
    msg =  {
        'reason': etype,
        'data': description
    }
    msg.update( additional_data )
    
    return HttpResponse( 
    
        json.dumps({
        
            'status' : 'fail',
            'msg': msg
        
        })
    
    )

def ajax_confirm( data={} ):

    return HttpResponse(

        json.dumps({
            
            'status' : 'ok',
            'msg': data
        
        })  
    )  

def auth_ajax_view( function=None, default_error=ajax_error() ):
    '''
    Decorator to send back a error in json if something goes wrong
    '''

    def _wrapped_view(request, *args, **kwargs):
        if not request.user.is_authenticated:
            return ajax_error('AuthError', 'User is not authenticated')

        try: 
            return function(request, *args, **kwargs)

        except:
            dbg_error = ajax_error('ScriptException', traceback.format_exc() )
            return dbg_error 

    return _wrapped_view

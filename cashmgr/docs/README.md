TODO
====

To fix:
    - add lookup strings in the transactions view
    - starting balances / corrections

From scratch:
    - invoices
    - checks printing/images/centre
    - check number on the transaction model
    - csv parsing interface (CSV format model?)
    - cash expenses?
    - pdf check printing: http://www.devshed.com/c/a/Python/Python-for-PDF-Generation/


The csv interface
-----------------

When reading a csv, the user can specify the columns.
Needs a general method to parse the csvs

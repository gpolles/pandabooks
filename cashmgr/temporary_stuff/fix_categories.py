names = [c.name for c in Category.objects.all()]

for s in names:
    lvls = s.split(':')
    for i, lvl in enumerate(lvls):
        me = ':'.join(lvls[:i+1])
        mec = Category.objects.filter(name=me)
        if len(mec) == 0:
            nc = Category(name=me)
            nc.save()
        else:
            nc = mec[0]
        if i > 0:
            up = ':'.join(lvls[:i])
            cts = Category.objects.get(name=up)
            nc.parent_category = cts
            nc.save()

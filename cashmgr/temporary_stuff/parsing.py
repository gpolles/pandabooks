
names = ['smartnfinal',
 'amazon',
 'arco',
 'dmv',
 'uber',
 'capital one',
 'university of so',
 'sprint',
 'rite aid',
 'ralphs',
 'chase card',
 'us dos visa fee aldershot',
 'umami burger',
 'non-chase atm fee',
 'non-chase atm withdraw',
 '7-eleven',
 'adidas',
 'converse',
 'columbia sportswear',
 'guess',
 'perry ellis',
 'br factory',
 'lamajoon',
 'usc hospitality retail',
 'isa japanese restauran',
 'rave 1084',
 'coles original french',
 "togo's",
 'wholefds',
 'ra marina del rey co',
 'tom bergins',
 'city of culver city bp',
 'united oil',
 'parking',
 'settebello',
 'coffe bean',
 'einstein bros',
 "busby's",
 'mega pizza and grill',
 'shell',
 'flv wine and spirits',
 'cosmo chandelier',
 'maverik',
 'forscher bakery',
 'silver eagle',
 'big fish family restaur',
 'bryce canyon restaurant',
 'phillips 66',
 'big johns texas bbq',
 'kenstour',
 'co river disc & cafe',
 'aramark lp driftwood lo',
 'westerners grocery',
 'safeway',
 'the view restaurant',
 'monument vly trade p',
 'monumnt vly nav',
 'chevron',
 'el tovar',
 'gc theatre venture',
 'dbg admissions phoenix az',
 'natural grocers',
 "gertrude's",
 'marriott',
 'pf changs',
 'circle k',
 'draft republic',
 "landini's 2",
 "lil piggy's bar-b-q",
 'coronado brewing co',
 'uss midway museum',
 'kaiser k. korki',
 'cheesecake',
 'wdch cafe',
 'auto park 16 disney',
 'rascal',
 'chase quickpay electronic transfer',
 'costco',
 'ucla',
 'atm withdrawal',
 'portos bakery',
 'ikea',
 'atm check deposit',
 'gate retail',
 'oxxo',
 'starbucks',
 '1212 santa monica',
 'hotel erwin',
 'pala vista gas',
 'wurstkuche',
 'marina mini market',
 'frenchcrepe',
 'lax airport',
 "papa john's",
 'ronnies',
 '90 west lounge',
 '1 gateway plaza/lacmta',
 'bristol farms',
 'lyft',
 'north shore poke',
 'barnesnoble',
 'yard house',
 'pampas grill',
 'ye olde kings',
 'pavillion',
 'chilis gosford',
 'pieology',
 'snpla van nuys',
 'rays & stark bar',
 'tobacco and accessor',
 'orange coast winery',
 'pavilions',
 'sq *angel city brew',
 'sq *dogtown dogs llc',
 'sage hyundai',
 'lorena sanchez',
 'mexicano',
 'atm cash deposit',
 'ross',
 'cvs',
 'enjoeat',
 'le pain quotidien',
 'parkin',
 'los balcones',
 'kings row',
 "trader joe's",
 'la wine co.',
 'usc transportation',
 'indo phili',
 'sushi time',
 '76 ',
 'todds pizza factory',
 'wal-mart',
 'sherman way oil',
 'patrick molloys',
 'la dijonaise',
 'wildcraft',
 'ekkamai',
 "pearl's liquor",
 'iyengar',
 "domino's",
 'ihop',
 'coca cola',
 'the rocking dead',
 'sq *bright angel',
 'treasure island',
 'sm-dwntwn struct',
 "tequila jack's",
 'sq *philz coffee',
 'jack in the box',
 'snow white cafe',
 'usc bookstore',
 'sprouts',
 'coffee bean',
 'the counter',
 'the chinese theatre',
 'hollywood and highland',
 '10 e restaurant',
 'yalla',
 'malibu wines',
 'grill concepts',
 'city of sm-structur',
 'cafe crepe',
 'joshua tree visitor',
 "denny's",
 'lure',
 'city tavern',
 'the wallace',
 'miyako',
 "father's office",
 'revolutionario',
 'east borough',
 'culver rest & bar',
 'venice beach wines',
 'usc trojan event',
 'kreation',
 'las perlas',
 'new school of cook',
 'beverage warehouse',
 "brennan's",
 'the canyon bistro',
 'cafe mimosa',
 'malaka brothers',
 'tanners',
 'literati',
 'ugo cafe',
 'century theatres',
 'circus circus',
 'margaritaville',
 'fuel bar',
 'kona grill',
 "ti gilley's",
 'coffee getty',
 'ace hotel',
 'tca fastrak',
 'thrifty',
 'toms shoes',
 'gant usa',
 'hagop',
 'cafe del rey',
 'c&m cafe',
 'lush',
 'love s country',
 'burger king',
 'check or supply',
 'taco bell',
 'otium on the go',
 'vespaio',
 'usc pharmacy',
 'bacaro',
 't-mobile',
 'barneys',
 'bubba gump']


def alignment_score_w_matrix(name, description):
    skip = -2
    match = 1
    substitution = -4
    m = np.empty((len(description), len(name)))
    p = np.empty((len(description), len(name), 2), dtype=int)
    for i in range(len(description)):
        if description[i] == name[0]:
            m[i, 0] = 1
        else: 
            m[i, 0] = skip
        p[i, 0] = (i, -1)
    for j in range(1, len(name)):
        m[0, j] = m[0, j-1] + skip
        p[0, j] = (0, j-1)
    for i in range(1, len(description)):
        for j in range(1, len(name)):
            m[i, j] = -200000
            s = m[i-1, j] + skip 
            if s > m[i, j]:
                m[i, j] = s
                p[i, j] = (i-1, j)
            s = m[i, j-1] + skip 
            if s > m[i, j]:
                m[i, j] = s
                p[i, j] = (i, j-1)
            if name[j] == description[i]:
                s = m[i-1, j-1] + match 
            else:
                s = m[i-1, j-1] + substitution
            if s > m[i, j]:
                m[i, j] = s
                p[i, j] = (i-1, j-1)
    mi = np.argmax(m[:, -1])
    s = m[mi, -1]
    return s, m, p


def show_align(m, p, name, description):
    """ assumes len(description) > len(name) """
    s0 = description
    i, j = np.argmax(m[:, -1]), len(name) - 1
    s1 = ''
    for k in range(len(s0)-1-i):
        s1 += '-'
    li, lj = p[i, j]
    while True:
        li, lj = p[i, j]
        if lj == -1:
            if m[i, j] >= 0:
                s1 += name[0]
            else:
                s1 += '-'
            break
        if li == i-1:
            if lj == j:
                s1 += '-'
            else:
                s1 += name[j]
        i, j = li, lj
    for k in range(i):
        s1 += '-'
    s1 = s1[::-1] 
    print (s0)
    print (s1)
    



def chase_parser(filename, names):
    checks = [x for x in db if x['Details'] == 'CHECK']
    non_checks = [x for x in db if x['Details'] != 'CHECK']

    for item in non_checks:
        naive_date = datetime.strptime(item['Posting Date'], '%m/%d/%Y')  
        date = naive_date.replace(tzinfo=timezone('US/Pacific'))
        description = item['Description'].lower()
        recognized = False
        payee = None
        for n in names:
            if description.find(n) != -1:
                recognized = True
                print ('> found payee for "{}" : {}'.format(description, n))
                payee = n
                break
        if not recognized:
            inn = input('"{}" has not been found. '.format(description) +
                        'Add it as (press enter to keep like it is): ')
            if inn == "":
                names.append(description)
            else:
                names.append(inn)



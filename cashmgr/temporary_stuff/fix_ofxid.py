import ofxparse
from cashmgr.models import Transaction
from datetime import datetime
import pytz
from decimal import Decimal
import numpy as np
from django.db.transaction import atomic
    
@atomic
def saveall(ts):
    for t in ts:
        t.save()

def make_aware(naive, mytz='US/Pacific'):
    naive = datetime.combine(naive, datetime.min.time())
    return pytz.timezone(mytz).localize(naive, is_dst=None)

def fix_ofxid(account, file):
    ofx = ofxparse.OfxParser.parse(open(file))
    ofxtransactions = ofx.account.statement.transactions
    ofxamounts = [t.amount for t in ofxtransactions]
    ofxdates = [make_aware(t.date) for t in ofxtransactions]
    transactions = Transaction.objects.filter(account=account)
    dates = [make_aware(d.replace(tzinfo=None)) for d in [t.date for t in transactions]]
    amounts = [Decimal('%.2f' % t.amount) for t in transactions]

    mask = np.array([True]*len(ofxamounts))
    def find_partner(d, a, dates, amounts, mask):
        opt = (np.array(amounts) - a) == 0
        deltas = np.abs([(d-dx).days for dx in dates])
        deltas[np.logical_not(opt)] = 100000
        deltas[np.logical_not(mask)] = 100000
        return np.argmin(deltas)

    for i in range(len(transactions)):
        x = find_partner(dates[i], amounts[i], ofxdates, ofxamounts, mask)
        mask[x] = False
        transactions[i].ofxid = ofxtransactions[x].id

    saveall(transactions)
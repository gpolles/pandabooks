
from .models import Transaction, Payee
import pytz
import uuid
from django.conf import settings
from .utils import parse_aware

def assign_payee_if_matches(transactions, payees):
    
    modified_transactions = []
    for i, transaction in enumerate(transactions):
        description = transaction.description.lower()
        if len(description) < 2 or transaction.ttype == 'check':
            continue

        confident_candidates = []
        # unsure_candidates = []

        for payee in payees:
            lookup_strings = [s for s in payee.lookup_strings.split(',')
                              if len(s) > 0]
            for ls in lookup_strings:
                if ls in description:
                    confident_candidates.append(payee)
                    break

        if len(confident_candidates) == 1:   # single confident candidate
            assigned_payee = confident_candidates[0]
            transaction.payee = assigned_payee
            if assigned_payee.default_category is not None:
                transaction.category = transaction.payee.default_category
            modified_transactions.append(transaction)
        # elif len(confident_candidates) > 1:  # multiple candidates
        #     modified_transactions.append(confident_candidates)
        # elif len(unsure_candidates) > 0:     # only unsure options
        #     modified_transactions.append(unsure_candidates)
        # else:    # no options
        #     continue

    return modified_transactions
    

def update_assignment(transactions, payee):
    ''' 
    when we add or modify a payee, we lookup the transactions
    without one, and assign if they fit.
    '''
    transactions = transactions.filter(payee=None)
    modified_transactions = assign_payee_if_matches(transactions, [payee])
    for transaction in modified_transactions:
        if not isinstance(transaction, Transaction):
            continue
        transaction.save()
    return modified_transactions


def parse_ofx(ofx, account):
    
    ids = {}
    
    # Note that, for example, the foreign fees have the same ofxid 
    # than the transaction itself, so ofxid's are not really unique :/
    # We have to check also the amounts for duplicates.
    
    for t in Transaction.objects.filter(account=account):
        if t.ofxid not in ids:
            ids[t.ofxid] = []    
        ids[t.ofxid].append(t.amount)

    new_transactions = []
    for t in ofx.account.statement.transactions:
        # skip if it is already there
        if t.id in ids:
            is_already_recorded = False
            for amnt in ids[t.id]:
                if abs(float(t.amount) - amnt) < 0.01:
                    is_already_recorded = True
                    break
            if is_already_recorded:
                continue

        date_aware = pytz.timezone(settings.TIME_ZONE).localize(t.date, 
                                                                is_dst=None)
        try:
            checknum = int(t.checknum)
        except ValueError:
            checknum = None 
        transaction = Transaction(ofxid=t.id,
                                  user=account.user,
                                  date=date_aware,
                                  ttype=t.type,
                                  description=(t.payee + t.memo).lower(),
                                  amount=float(t.amount),
                                  check_number=checknum,
                                  account=account)
        new_transactions.append(transaction)

    assign_payee_if_matches(new_transactions, Payee.objects.filter(user=account.user))
    
    # bulk create does not call save, so the incomplete field will not be 
    # correctly set
    for t in new_transactions:
        t.incomplete = t.is_incomplete()
        
    Transaction.objects.bulk_create(new_transactions)
    
    return len(ofx.account.statement.transactions), len(new_transactions)

def parse_csv(account, text, columns_mapping, delim=';', skip_header=1):
    new_transactions = []
    lines = text.split('\n')

    for i, line in lines:
        
        if i < skip_header:
            continue
        values = line.split(delim)

        if 'ttype' in columns_mapping:
            ttype = str(values[columns_mapping['ttype']]).lower()
        else:
            ttype = 'other'

        if 'check_number' in columns_mapping:
            try:
                check_number = int(values[columns_mapping['check_number']])
            except ValueError:
                check_number = None
        else:
            check_number = None

        if 'amount' in columns_mapping:
            # single column with positive and negative values 
            try:
                amount = float(values[columns_mapping['amount']])
            except ValueError:
                amount = 0
        else:
            # we have two separate columns for income and expenses
            vin = values[columns_mapping['income']]
            vout = values[columns_mapping['expenses']]
            # vin should always be positive, vout negative
            try:
                vin = float(vin)
                if vin < 0:
                    vin = -vin
            except ValueError:
                vin = 0.0
            try:
                vout = float(vout)
                if vout > 0:
                    vout = -vout
            except ValueError:
                vout = 0.0
            amount = vin + vout

        transaction = Transaction(ofxid=str(uuid.uuid4()),
                                  user=account.user,
                                  date=parse_aware(values[columns_mapping['date']]),
                                  ttype=ttype,
                                  description=str(values[columns_mapping['description']]),
                                  amount=amount,
                                  check_number=check_number,
                                  account=account)

        new_transactions.append(transaction)

    assign_payee_if_matches(new_transactions, Payee.objects.filter(user=account.user))
    
    # bulk create does not call save, so the incomplete field will not be 
    # correctly set
    for t in new_transactions:
        t.incomplete = t.is_incomplete()
        
    Transaction.objects.bulk_create(new_transactions)
    
    return len(lines) - skip_header, len(new_transactions)
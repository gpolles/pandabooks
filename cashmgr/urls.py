from django.conf.urls import url, include
from django.urls import path
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    path('accounts/', include('django.contrib.auth.urls')),
    url(r'^incomplete/$', views.incomplete_transactions, name='index'),
    url(r'^add_account/$', views.add_account_dialog, name='Add Account Dialog'),
    url(r'^add/payee/$', views.add_payee, name='Add Payee'),
    url(r'^add/invoice/$', views.add_invoice, name='New Invoice'),
    url(r'^add/category/$', views.add_category, name='Add Category'),
    url(r'^add/account/$', views.add_account, name='Add Account Routine'),
    url(r'^import/$', views.upload_file, name='Upload a csv file'),
    url(r'^accounts/(?P<account_id>[0-9]+)/$', views.account_view, name='Account main view'),
    #url(r'^inline_payee_change/(?P<transaction_id>[0-9]+)/$', views.payee_change, name='ajax payee change'),
    url(r'^quick_add/(?P<transaction_id>[0-9]+)/$', views.quick_add, name='ajax quick add'),
    url(r'^delete/transaction/(?P<transaction_id>[0-9]+)/$', views.delete_transaction, name='delete transaction'),
    #url(r'^inline_category_change/(?P<transaction_id>[0-9]+)/$', views.category_change, name='ajax category change'),
    url(r'^inline/modify/transaction/(?P<transaction_id>[0-9]+)/$', views.modify_transaction_inline, name='ajax transaction change'),
    #url(r'^category/exists/$', views.category_exists, name='ajax category check'),
    #url(r'^payee/exists/$', views.payee_exists, name='ajax payee check'),
    url(r'^transactions/$', views.list_transactions, name='Transaction List'),
    url(r'^invoices/$', views.invoice_list, name='Invoice List'),
    url(r'^report_center/$', views.report_center, name='Report Center'),
    url(r'^report/$', views.create_report, name='Report'),
    url(r'^payees/$', views.payee_center, name='Payee'),
    url(r'^modify/payee/(?P<uid>[0-9]+)/$', views.modify_payee, name='Modify Payee'),
    url(r'^modify/invoice/(?P<pk>[0-9]+)/$', views.add_invoice, name='Modify Invoice'),
    url(r'^details/payee/(?P<uid>[0-9]+)/$', views.show_payee, name='Payee details'),
    #url(r'^recordinfo/(?P<model>\w+)/(?P<pk>[\w\W]+)/$', views.record_info, name='Record Info'),
    #url(r'^change/(?P<model>\w+)/(?P<pk>[\w\W]+)/(?P<ftype>\w+)/(?P<field>\w+)/$', views.change_field, name='Change field'),
    url(r'^preview/invoice/$', views.preview_invoice, name='Invoice Preview'),
    url(r'^query/record/$', views.query_record, name='Query a record'),
    
    
]

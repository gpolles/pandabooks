from django.forms import ModelForm
from .models import Account, Payee, Category, MyCheck, Invoice, MyCompany
from django import forms


class Html5DateInput(forms.DateInput):
    input_type = 'date'


class UploadOfxForm(forms.Form):
    file = forms.FileField(label='OFX file')


class AccountForm(ModelForm):
    class Meta:
        model = Account
        fields = '__all__'


class PayeeForm(ModelForm):
    class Meta:
        model = Payee
        fields = ['name', 'lookup_strings']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'payeefield'}),
        }


class CategoryForm(ModelForm):
    class Meta:
        model = Category
        fields = ['name']
        widgets = {
            'name': forms.TextInput(attrs={'class': 'categoryfield'}),
        }


class MyCheckForm(ModelForm):
    class Meta:
        model = MyCheck
        fields = ['number', 'date_created', 'amount', 'to_print']


class InvoiceForm(ModelForm):
    class Meta:
        model = Invoice
        fields = ['recipient_address', 'recipient_email', 'date_created', 'date_due', 'notes', 'paid']
        widgets = {
            'recipient_address': forms.Textarea(attrs={'rows':3}),
            'date_created': Html5DateInput(),
            'date_due': Html5DateInput()
        }


class MyCompanyForm(ModelForm):
    class Meta:
        model = MyCompany
        fields =  '__all__'
        widgets = {
            'smtp_password': forms.PasswordInput(),
        }

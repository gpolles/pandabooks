function guid(){
  return uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
    var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
    return v.toString(16);
  });

} 


function open_modal(options) {

  var options = options || {};
  var title = options.title || '?';
  var body = options.body || '?';
  var on_ok = options.on_ok || function(){};
  var on_cancel = options.on_cancel || function(){};

  BaseModal.title.text(title);
  BaseModal.body.text(body);
  BaseModal.on_ok = on_ok;
  BaseModal.on_cancel = on_cancel;
  BaseModal.show();

}

function confirm_dialog(title, text, dialog_function, cancel_function, dom_elem){

    $('<div></div>').dialog({
	    modal: true,
	    title: title,
	    open: function() {
	    	$(this).html(text);
	    },
	    position: {my: "center", at: "center", of: dom_elem},
	    buttons: {
	    	"Confirm": function() {
		        dialog_function();
		        $( this ).dialog( "close" );
	        },
	      	Cancel: function() {
		        cancel_function();
		        $( this ).dialog( "close" );
	        }
	    }
    });
}


function getProperty(o, prop, default_value=null) {
    if (o[prop] !== undefined) return o[prop];
    else return default_value;
}


function confirm_dialog2(options){
  var title = getProperty(options, "title", "dialog");
  var text = getProperty(options, "text", "Are you sure?");
  var callback_ok = getProperty(options, "ok", function(){});
  var callback_cancel = getProperty(options, "cancel", function(){});
  var center_on = getProperty(options, "center_on", window);
  var modal = getProperty(options, "modal", true);
  var args = getProperty(options, "args", []);

  var element = $('<div></div>');
  element.dialog({
    modal: modal,
    title: title,
    open: function() {
      $(this).html(text);
    },
    position: {my: "center", at: "center", of: center_on},
    buttons: {
      Ok: function() {
        callback_ok.apply(this, args);
        $( this ).dialog( "close" );
      },
      Cancel: function() {
        callback_cancel.apply(this, args);
        $( this ).dialog( "close" );
      },
    }
  });
  return element;
}



function setup_autocomplete_fields( options ) {

  var options = options || {};

  var payee_change_callback = options.payee_callback || function() {};
  var category_change_callback = options.category_callback || function() {};
  var pnames = options.payee_names || ( typeof jsdata !== 'undefined' ? jsdata['payee_names'] : [] ) ;
  var cnames = options.category_names || ( typeof jsdata !== 'undefined' ? jsdata['category_names'] : [] );

  $( ".payeefield, .categoryfield" ).focus( function () {
      $( this ).attr('originalvalue', $( this ).val());
  });

  $( ".payeefield" ).autocomplete({
    source: function (request, response) {
      response($.ui.autocomplete.filter(pnames, request.term));
    },
  });

  $( ".categoryfield" ).autocomplete({
    source: function (request, response) {
      response($.ui.autocomplete.filter(cnames, request.term));
    },
  });

  $( ".payeefield" ).blur( function(e) {
      var edt = $( this );
      var new_name = edt.val();
      if (new_name == $( this ).attr('originalvalue')){return;}
    	if (new_name.length == 0){
    		edt.val($( this ).attr('originalvalue')); 
    		return;	
    	}
      payee_change_callback( $( this ) );
  });

  $( ".categoryfield" ).blur( function(e) {
      var edt = $( this );
      var new_name = edt.val();
      if (new_name == $( this ).attr('originalvalue')){return;}
    	if (new_name.length == 0){
    		edt.val($( this ).attr('originalvalue')); 
    		return;	
    	} 
      category_change_callback( $( this ) );
  });
}


// Listen to update message from child window
// eventer(messageEvent,function(e) {
//     msg = JSON.parse(e.data);
//     if (msg.action == 'update'){
//         p = $( '#pay_' + msg.id );
//         jQuery.ajax({
//             url: '/cashmgr/inline_payee_change/' + msg.id + '/?payee=' + encodeURIComponent(msg.payee),
//             dataType: 'json',
//             success: function (result) {
//                 if (result.status == "empty"){
//                     edt.val(result.lastp);
//                 }
//                 if (result.status == "ok"){
//                     $( '#cat_' + t_id ).val(result.newcat);
//                 }
//                 if (result.status == "query"){
//                     console.log('error');
//                     edt.val(result.lastp);
//                 }
//             },
//             async: false
//         });
//     }
// },false);

//cross site forgery protection stuff
$(function() {
    // This function gets cookie with a given name
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');

    /*
    The functions below will create a header with csrftoken
    */

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    function sameOrigin(url) {
        // test that a given url is a same-origin URL
        // url could be relative or scheme relative or absolute
        var host = document.location.host; // host + port
        var protocol = document.location.protocol;
        var sr_origin = '//' + host;
        var origin = protocol + sr_origin;
        // Allow absolute or scheme relative URLs to same origin
        return (url == origin || url.slice(0, origin.length + 1) == origin + '/') ||
            (url == sr_origin || url.slice(0, sr_origin.length + 1) == sr_origin + '/') ||
            // or any other URL that isn't scheme relative or absolute i.e relative.
            !(/^(\/\/|http:|https:).*/.test(url));
    }

    $.ajaxSetup({
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type) && sameOrigin(settings.url)) {
                // Send the token to same-origin, relative URLs only.
                // Send the token only if the method warrants CSRF protection
                // Using the CSRFToken value acquired earlier
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

});


function display_category( name ) {
  return name.split(':').join(' <i class="fas fa-caret-right"></i> ');
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function setup_sortable_headers( currentField, currentDir ) {

  var url = window.location.href.split('?')[0];
  // get the current parameters passed to the page
  // for example if there are filters, we want to keep them
  // so we filter the rest

  var queries = window.location.search.substr(1).split("&");
  var filteredQueries = [];

  for ( i = 0, l = queries.length; i < l; i++ ) {
    
    var temp = queries[i].split('=');

    if ( temp[0] == 'orderby' ) {

      currentField = decodeURIComponent(temp[1]);
      console.log('currentField: ' + currentField );

    } else if ( temp[0] == 'orderdir' ) {

      currentDir = decodeURIComponent(temp[1]);
      console.log('currentDir: ' + currentDir );
      
    } else {

      console.log( 'other: ' + temp[0] + ' ' + temp[1] );
      filteredQueries.push(queries[i]);

    }

  }
  
  $('.sort-switch').each( function(idx, el) {

    var sortField = $( el ).attr('sort-field');
    var sortDirection = 'asc';
    if ( sortField == currentField )
      sortDirection = currentDir == 'asc' ? 'des' : 'asc';
    
    var target_url = url + '?orderby=' + encodeURIComponent(sortField);
    target_url += '&orderdir=' + sortDirection;
    if ( filteredQueries.length )
      target_url += filteredQueries.join('&');
    
    // set the target_url
    $( el ).attr('href', target_url);
    // set the arrow if current item
    if ( sortField == currentField ){

      var arrow = currentDir == 'asc' ? '<i class="fas fa-chevron-up"></i> ' : '<i class="fas fa-chevron-down"></i> ';
      $( el ).html( arrow + $( el ).text() );

    }
  
  });

}
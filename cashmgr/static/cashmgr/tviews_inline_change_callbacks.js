

// Add format to string prototype.
if (!String.prototype.format) {
  String.prototype.format = function() {
    var args = arguments;
    return this.replace(/{(\d+)}/g, function(match, number) { 
      return typeof args[number] != 'undefined'
        ? args[number]
        : match
      ;
    });
  };
}



function request_add_record( rtype, new_name, t_id, edt ) {

  var url = '/cashmgr/add/{0}/?transaction={1}&name={2}'.format( rtype, t_id, encodeURIComponent(new_name) );

  open_modal({
    title: 'The {0} is not in the database'.format( rtype ),
    body: 'Do you want to create a new {0} named "{1}"?'.format( rtype, new_name ),
    on_ok: function open_window() { // called on dialog confirmation
      window.open( url, '_blank');
      BaseModal.hide();
    }
  });

}


function handle_result( edt, rtype ) {

  var inner = function( result ) {

    console.log(result);

    // if successful, send to the window to handle the updates
    if ( result.status == "ok" )
      window.postMessage(result.msg, '*');   

    if ( result.status == "fail" ) {

      // first, rollback to the last value
      var new_name = edt.val(); 
      var t_id = edt.attr('chid');
      edt.val( edt.attr("valid_value") ); 
      
      var reason = result.msg.reason;

      // if we cannot find the record in the database, we ask if we want to add it
      if ( reason.toLowerCase() == rtype + "doesnotexist" )
        request_add_record( rtype, new_name, t_id, edt );
    
    }

  }

  return inner;

}


function edt_change_callback( rtype ) {

  var inner = function( edt ) {

    var new_name = edt.val();

    if ( new_name.length == 0 )
      return false
    
    var t_id = edt.attr('chid');

    jQuery.ajax({
      url: '/cashmgr/inline/modify/transaction/{0}/?{1}='.format( t_id, rtype ) + encodeURIComponent(new_name),
      dataType: 'json',
      success: handle_result( edt, rtype ),
      async: false // keep async - "modal execution"
    });

  }

  return inner;

}


// function payee_change_callback(edt) {
//   var new_name = edt.val();
//   if ( new_name.length == 0 )
//     return false
//   var t_id = edt.attr('chid');

//   var handle_result = function( result ) {

//     // if successful, send to the window to handle the updates
//     if ( result.status == "ok" )
//           window.postMessage(result.msg, '*');   

//     if ( result.status == "fail" ) {

//       // first, rollback to the last value
//       edt.val( edt.attr("valid_value") ); 

//       var reason = result.msg.reason;

//       // if we cannot find the record in the database, we ask if we want to add it
//       if ( reason.lower() == rtype + "doesnotexist" )
//         request_add_record( rtype, new_name, t_id, edt )
    
//   }

//   jQuery.ajax({
//     url: '/cashmgr/inline_payee_change/' + t_id + '/?payee=' + encodeURIComponent(new_name),
//     dataType: 'json',
//     success: function (result) {  
             
      
//       // errors
//       if ( result.status == "fail" ) {

//         var errdata = result.msg;
        
//         if (errdata.reason == "EmptyString"){ // invalid data sent, rollback
//           edt.val( edt.attr("valid_value") ); 
//         }
//         if (errdata.reason == "PayeeDoesNotExist"){ 
//           // the payee is not in the database, asks the user to add it or 
//           // to rollback.
//           edt.val(edt.attr("valid_value"));
          
//         }
//       }
//     },
//     async: false
//   });
// }

// function category_change_callback(edt) {
//   var new_name = edt.val();
//   var t_id = edt.attr('chid');
//   jQuery.ajax({
//     url: '/cashmgr/inline_category_change/' + t_id + '/?name=' + encodeURIComponent(new_name),
//     dataType: 'json',
//     success: function (result) {      
//       if (result.status == "ok"){
//         // send to self the necessary updates
//         window.postMessage(result.msg, '*');        
//       }else{
//         var errdata = result.msg;
//         if (errdata.reason == "EmptyString"){
//           edt.val(errdata.rollback[0].value);
//         }
//         if (errdata.reason == "CategoryDoesNotExist"){
//           edt.val(errdata.rollback[0].value);
//           confirm_dialog( 
//             'The category is not in the database.', 
//             'Do you want to create a new category named "' + new_name + '"?',
//             function() { // called on dialog confirmation
//               window.open(
//                 '/cashmgr/add/category/?transaction=' + t_id + '&name=' + encodeURIComponent(new_name),
//                 '_blank',
//                 'height=260,width=450'
//               );
//             },
//             function() {}, // called on dialog dismiss
//             edt // position od dialog
//           );
//         }
//       }
//     },
//     async: false
//   });
// }

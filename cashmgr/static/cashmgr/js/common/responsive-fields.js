function open_modal(options) {

  var options = options || {};
  var title = options.title || '?';
  var body = options.body || '?';
  var on_ok = options.on_ok || function(){};
  var on_cancel = options.on_cancel || function(){};

  BaseModal.title.text(title);
  BaseModal.body.text(body);
  BaseModal.on_ok = on_ok;
  BaseModal.on_cancel = on_cancel;
  BaseModal.show();

}

function request_add_record( opts ) {

  var opts = opts || {};
  var model = getProperty(opts, 'model', null);

  var url = `/cashmgr/add/${model}/`;
  var queries = [];
  for ( var key in opts ) {
    if ( key == 'model' ) continue;
    queries.push( encodeURIComponent( key ) + '=' + encodeURIComponent( opts[key] ) );
  }
  if ( queries.length )
    url += '?' + queries.join('&');

  open_modal({
    title: `The ${model} is not in the database`,
    body: 'Do you want to create a new record?',
    on_ok: function open_window() { // called on dialog confirmation
      window.open( url, '_blank');
      BaseModal.hide();
    }
  });

}


function responsive_edt( edt, on_change ) {

  var options = options || {};
  var on_change = on_change || function() {};

  // save original value
  edt.focus( function () {
      $( this ).attr('originalvalue', $( this ).val());
      $( this ).select();
  });

  // if changed call callback. 
  edt.blur( function() {

    var caller = $( this );
    var new_name = caller.val();
    var last_value = caller.attr( 'originalvalue');
    
    if ( new_name == last_value )
      return;
    
    on_change( caller, new_name, last_value ) 

  });

}


function confirm_value( edt, value ) {

  edt.val(value);
  edt.attr("valid_value") = value;

}

function rollback( edt ) {

  edt.val( edt.attr("valid_value") );

}

function handle_ajax_result( options ) {

  var options = options || {};
  var on_ok = options.on_ok || function( result ){
    window.postMessage(result.msg, '*');
  };
  var on_fail = option.on_fail || {
    default : function( result ) {
      alert( 'We hit an error: ' + result.msg.reason );
    }
  };
  
  var inner = function( result ) {

    console.log(result);

    // if successful, send to the window to handle the updates
    if ( result.status == "ok" )
      on_ok( result );  

    if ( result.status == "fail" ) {

      var retfun = on_fail[ result.msg.reason ] || on_fail.default; 
      retfun( result );
    
    }

  }

  return inner;

}


function check_if_exist( opts ) {
  opts = opts || {};
  var model = getProperty(opts, 'model', '')
  var field = getProperty(opts, 'field', '')
  var value = getProperty(opts, 'value', '')
  var on_ok = getProperty(opts, 'ok', function(){});
  var on_fail = getProperty(opts, 'fail', function(){});


  if ( field.length == 0 )
    return false

  jQuery.ajax({

    url: '/cashmgr/query/record/',
    
    data: {
    
      model: model,
      field: field,
      value: value,
    
    },
    
    dataType: 'json',
    
    success: function( response ) {

      console.log( response );

      if ( response.status == 'ok' )
        // return the record
        on_ok( response.msg );
      
      else
        // if does not exist, pass the data to the callback
        on_fail( data );

    },

  });

}
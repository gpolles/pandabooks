
function CsvLoader(container) {

  const self = this;
  const coltype = ['date', 'description', 'amount', 'check_number', 'income', 'expenses', 'transaction_type'];


  self.delim = ',';
  self.container = $(container);
  self.lines = [];
  self.selects = [];
  self.ncols = 0;
  self.cmap = {};
  self.skip = 0;

  for (var i = 0; i < coltype.length; i++) {
    self.cmap[coltype[i]] = -1;
  }

  self.container.html(`
    <input type="file" id="csv_input">
    <div><label>Delimiter:</label><input type="text" id="csv_delim_edt" value="${self.delim}" length="2"/></div>
    <div><label>Skip:</label><input type="number" id="csv_skip_edt" value="${self.skip}" /></div>
    <div>
    <table border=1>
    No file loaded yet.
    <thead></thead>
    <tbody></tbody>
    </table>
    </div>
  `);
  
  self.updateTableHeader = function() {
    thead = $('thead', self.container);
    thead.html('');
    self.selects = [];
    for (let i = 0; i < self.ncols; i++) {
      var th = $('<th></th>');
      var s = $(`<select id="csv_map_col_${i}"></select>`);
      s.append($(`<option value="" selected>----</option>`));
      for (var j = 0; j < coltype.length; j++) {
        s.append($(`<option value="${coltype[j]}"">${coltype[j]}</option>`));
      }
      // set the mapping when the value changes.
      s.on('change', function() {
        self.cmap[$(this).val()] = i;
      });
      self.selects.push(s);
      th.append(s);
      thead.append(th);
    }

    for (var i = 0; i < coltype.length; i++) {
      var cc = self.cmap[coltype[i]];
      if ( cc >= 0 && cc < self.ncols) {
        self.selects[cc].val(coltype[i]); 
      }
    }
  }

  self.updateTableBody = function() {
    
    tbody = $('tbody', self.container);
    tbody.html("");
    self.ncols = 0;

    for (var i = 0; i < self.lines.length; i++) {
      var tr = $('<tr></tr>');
      tbody.append(tr);
      var values = self.lines[i].split(self.delim);
      for (var j = 0; j < values.length; j++) {
        tr.append(`<td>${values[j]}</td>`);
        self.ncols = Math.max(j+1, self.ncols);
      }
      if ( i < self.skip) {
        tr.css('background', 'grey');
      }
    }

  }

  self.updateTable = function() {
    self.updateTableBody(); // here we check the number of columns
    self.updateTableHeader(); // so we update the header after
  }

  self.onFileSelected = function() {
    var file = document.getElementById("csv_input").files[0];
    if (file) {
        var reader = new FileReader();
        var blob = file.slice(0, 10000);

        reader.onload = function (evt) {
          self.lines = evt.target.result.split("\n");
          if (self.lines)
            self.lines.pop();  // ignore last, possibly incomplete line
          self.lines = self.lines.slice(0, 15); // keep only a small number of rows as example
          self.updateTable();
        }
        reader.onerror = function (evt) {
            alert("error reading file");
        }

        reader.readAsText(blob, "UTF-8");
    }
  }

  //
  $('#csv_input', self.container).on('change', self.onFileSelected);

  $('#csv_delim_edt', self.container).on('input', function() {
    self.delim = $(this).val();
    self.updateTable();
  });

  $('#csv_skip_edt', self.container).on('input', function() {
    self.skip = $(this).val();
    self.updateTableBody();
  });

}


String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

template_string = ('<li class="tree-item tree-item-%level%">' +
	'%checkbox%' +
	' <label for="cs-%fullname" class="form-check-input"><a href="javascript:;" class="tree-item-a" node_id="%id%">' +
	' %name% %glyphicon% </a></label> ' +
	'</li>');


function render_checkbox(node){
	html = '<input type="checkbox" class="tree-item-cb form-check-label" id="cs-'
		+ node.getFullPath()
		+ '" name="cs-'
		+ node.getFullPath()
		+ '" isindeterminate="'
		+ node.indeterminate
		+ '" node_id="'
		+ node.id
		+ '" ';
	if (node.status == 1){
		html += 'checked';
	}
	return html + '/>'
}


function render_glyphicon(node){
	if (!node.hasChildren()){
		return '';
	}
	if (node.collapsed){
		return '<span class="glyphicon glyphicon-menu-right" style="font-size:0.7em"></span>'
	}else{
		return '<span class="glyphicon glyphicon-menu-down" style="font-size:0.7em"></span>'
	}
}

function parse_template(node){
	parsed = template_string.replaceAll(
			'%checkbox%', render_checkbox(node)
		).replaceAll(
			'%glyphicon%', render_glyphicon(node)
		).replaceAll(
			'%id%', node.id
		).replaceAll(
			'%level%', node.level
		).replaceAll(
			'%name%', node.name
		).replaceAll(
			'%fullname%', node.getFullPath()
		);
	return parsed;
}

function update_parents_status(node){

	if(node == false)
		return;

	var n_selected = 0;
	for (j in node.children)
		if (node.children[j].status == 1)
			n_selected++;

	if (n_selected == node.children.length){
		node.status = 1;
		node.elem.children("input")
			.prop('indeterminate', false)
			.prop('checked', true);
	}else if (n_selected == 0){
		node.status = 0;
		node.elem.children("input")
			.prop('indeterminate', false)
			.prop('checked', false);
	}else{
		node.status = -1;
		node.elem.children("input")
			.prop('indeterminate', true)
			.prop('checked', true);
		
	}
	update_parents_status(node.parent);
}


function tree_item_toogle_collapsed(node){
	node.collapsed = !node.collapsed
	if (node.collapsed){
		console.log('hide called');
		node.elem.children("ul").hide();
	}else{
		console.log('show called');
		node.elem.children("ul").show();
	}
	node.elem
		.children("label")
		.children("a")
		.children(".glyphicon")
		.replaceWith(render_glyphicon(node));
}


function tree_item_change_status(node){
	console.log('change status fired. Id:' + node.id);
	if (node.status == 0 || node.status == -1){
		node.status = 1;
		node.elem.find(".tree-item-cb")
			.prop('indeterminate', false)
			.prop('checked', true);
		node.setSubTreeStatus(1);
	}else{
		node.status = 0;
		node.setSubTreeStatus(0);
		node.elem.find(".tree-item-cb")
			.prop('indeterminate', false)
			.prop('checked', false);
		// set as undetermined all the parent checkboxes
		node.setSubTreeStatus(0);
	}
	update_parents_status(node.parent);
}




// Node class to track the information of each item
class tsNode{
	constructor(name, parent){
		this.parent = parent;
		if (parent){
			parent.addChild(this);
			this.level = parent.level + 1;
		}else{
			this.level = 0;
		}
		this.children = []
		this.indeterminate = false;
		this.name = name; 
		this.elem = false; // dom element
		this.collapsed = false;
		this.id = -1;
		this.status = 1
	}

	hasChildren(){
		return this.children.length != 0;
	}

	isLeaf(){
		return !this.hasChildren();
	}

	addChild(child_node){
		this.children.push(child_node);
	}

	getChild(name){
		for (var i = this.children.length - 1; i >= 0; i--) {
			if (name == this.children[i].name){
				return this.children[i];
			}
		}
		return false;
	}

	find(opath){
		// slice function copies the array,
		// shift function pops the front element.
		var path = opath.slice();
		var next = path.shift();

		var ch = this.getChild(next);
		
		if (ch == false)
			return false;
		
		if (path.length == 0)
			return ch;
		else
			return ch.find(path);

		return false;
	}

	getFullPath(delim=':'){
		var path = []
		var p = this;
		while (p.level > 0){
			path.unshift(p.name);
			p = p.parent;
		}
		return path.join(delim);
	}

	setSubTreeStatus(status){
		for (var j in this.children){
			this.children[j].status = status;
			this.children[j].setSubTreeStatus(status);
		}
	}


	// Recursively generates the html from
	// the node as a children li + ul of the 
	// specified element. 
	generateHtml(parent_element){
		var list_element = parent_element;
		if (this.level != 0){ 
			list_element = $(parse_template(this))
			parent_element.append(list_element);
		}
		this.elem = list_element;
		
		if (this.hasChildren()){
			var ulist = $(
				'<ul class="tree-select-ul" level="' +
				this.level +
				'"></ul>'
			);
			list_element.append(ulist)
			for (var j in this.children){
				this.children[j].generateHtml(ulist);
			}	
		}
	}

	sort(){
		this.children.sort(function(a, b){
			if(a.name < b.name) return -1;
			if(a.name > b.name) return 1;
			return 0;
		});
		for (var i = 0; i < this.children.length; i++) {
			this.children[i].sort()
		}
	}

	setup(){
		var tree = this;
		$('.tree-item-a').click(function(){
			var id = parseInt($(this).attr('node_id'));
			var node = tree.node_refs[id];
			tree_item_toogle_collapsed(node);
		})

		$('.tree-item-cb').click(function(){
			var id = parseInt($(this).attr('node_id'));
			var node = tree.node_refs[id];
			tree_item_change_status(node);
		})

	}

	setPadding(value, unit='px'){
		if(this.node_refs){
			for(i in this.node_refs){
				this.node_refs[i]
					.elem
					.children('ul')
					.css('padding-left',
						 value * this.node_refs[i].level + unit);
			}
		}
	}

}

// Build the tree from names of items
// delim is the subcategory special char
// i.e. the slash in paths.
function build_tree(names, delim=':'){
	tree = new tsNode('', false);
	node_refs = []
	for (var n in names){
		name = names[n];
		path = name.split(delim);
		base = tree;
		for (i=0; i<path.length; i++){
			next = base.getChild(path[i]);
			if (!next){
				var next = new tsNode(path[i], base);
				next.id = node_refs.length;
				node_refs.push(next);
			}
			base = next;
		}
	}
	tree.node_refs = node_refs;
	return tree;
}

function tree_select(div, names){
	var tree = build_tree(names);
	tree.sort();
	tree.generateHtml(div);
	tree.setPadding(20);
	tree.setup();

	return tree;
}


